---
layout: job_family_page
title: "Technical Writer"
---

At GitLab, our team of technical writers is responsible for ensuring that the [documentation](https://docs.gitlab.com/) for all of our products is clear, correct, and easy to use. We are looking for great writers with strong technical proficiencies who will help our users succeed with our [rapidly evolving suite of developer tools](https://about.gitlab.com/releases/).

You’ll collaborate with our engineers, who write the first draft of docs for the new features they create. You’ll dive in on special projects, authoring new content and working on new site features and processes. You’ll collaborate with others across the organization to craft tutorials and other educational resources. You’ll be at the leading edge of DevOps while contributing to one of the [world’s largest open-source projects](https://about.gitlab.com/2017/07/06/gitlab-top-30-highest-velocity-open-source/) and engaging with our wider community.

GitLab is an [all-remote company](https://about.gitlab.com/company/culture/all-remote/), and this is a remote position with no geographical requirements.

### Responsibilities

- **Continuously improve GitLab’s documentation content** in collaboration with engineers, product managers, and others.
    - Review and edit doc plans and content for all new and enhanced features.
    - Produce written and video tutorials for getting started with GitLab features and for specific use cases.
    - Help developers and other members of the community who have documentation-related questions.
    - Identify and address content gaps or the need for additional media such as diagrams or videos.
    - Participate in reviews and revamps of section or page content and structure.
    - Help review and triage incoming suggestions, corrections, and other content from the community.

- **Continuously improve GitLab’s documentation site features and user experience** in collaboration with engineers and other technical writers. This may include the documentation site’s design, search, build process, feedback methods, SEO, visitor analytics, versioning, and other technical components.
    - Contribute to the planning and code of new site features and enhancements.
    - Coordinate with frontend and backend engineers when their help is needed.

- **Contribute to the improvement of team process and style, as well as cross-functional efforts.**
    - Act as a reviewer of release blog posts and contributor to GitLab’s [Handbook](https://about.gitlab.com/handbook/).
    - Make it easy for contributors from the community, internal and external, to submit quality documentation.
    - Contribute to the documentation [Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html) and documentation [process guides](https://docs.gitlab.com/ee/development/documentation/).
    - Collaborate and improve upon collaborative processes with others including product managers, Support, Marketing, Engineering, and the wider GitLab community.

### Additional Responsibilities for Senior Technical Writers

- **Content**
    - Plan and lead major content initiatives such as a new site sections and wide-ranging content inventories.
- **Site features and user experience** 
    - Plan and lead major site feature enhancement initiatives.
    - Manage the team’s optimized use of certain third-party services. 
    - Produce reports on documentation usage and production metrics.
- **Leadership**
    - Mentor newly hired technical writers.
    - Act as a liaison with certain teams for our cross-functional efforts.
    - Represent the team at hackathons or other community endeavors.
    - Plan and lead a portion of the team’s process-improvement initiatives.
    - Organize and prioritize portions of the team's backlog and other needs.

### Requirements

- You have:
    - An affinity for managing and writing software documentation.
    - Excellent writing and editing skills.
    - Understanding of what makes documentation clear and effective.
    - Great teaching skills that translate into amazing written work.
    - Familiarity with the Linux or Mac command line.
    - Experience using Git, HTML/CSS, and at least one programming language (does not have to be from a professional context)
- You are:
    - Highly organized; able to triage and prioritize numerous issues and projects.
    - Able to succeed in a remote, globally distributed work environment.
- You share our [values](https://about.gitlab.com/handbook/values), and work in accordance with those values.

You’ll receive special consideration if you have experience with:
- Static site generators and managing docs as code.
- DevOps tools.
- JavaScript and intermediate front-end development.
- Advanced programming or other technical experience.

### Additional Requirements for Senior Technical Writers

- Experience working on documentation for a large variety of products and services.
- Experience planning and leading major initiatives such as the launch of new
documentation resources, large-scale documentation reviews and overhauls,
cross-functional initiatives, etc.
- Experience with static site generators, managing docs as code, and the nuances
of differing markup standards and rendering engines.
- Experience establishing processes used by teams across an organization in support
of documentation.
- Demonstration of the use of data and evidenced-based decision making.
- Experience with DevOps tools and some degree of front-end or back-end engineering.