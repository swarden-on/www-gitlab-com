---
title: "GitLab's Functional Group Updates: October 2nd - November 2nd" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/backend-discussion/2017-10-02/#1)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/9YsTEIHmBwU" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/gitlab-product-update-oct-3)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/QlCjFR6Jukw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/1EZ026lOKsURcsym8pg3497yDbSTcMslHQ9rE0iCDPtQ/edit#slide=id.g1c74eaa825_1_0)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/lQdTJAG2DUo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI/CD Team

[Presentation slides](https://docs.google.com/presentation/d/1ucmf5FH6fPVZ0gnG_MdX5l0Z8oc3siMw52KO9x2PtMg/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_o2-Hr84iYY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/edge/2017-10-31/#1)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/wwoeczh_FIE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1i6t2ofgxeUpmLzOqGyT-JcMAPuBu_tt18XAQuQDszEg/edit#slide=id.p)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/UxF0FpkrCXc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
