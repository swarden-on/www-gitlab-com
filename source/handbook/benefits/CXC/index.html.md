---
layout: markdown_page
title: "CXC"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

The following benefits are provided by [CXC](https://cxcglobal.com/) and apply to team members who are contracted through CXC. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at CXC.

## Australia

- CXC do provide private healthcare
- Contributions are made into [Superannuation](https://www.ato.gov.au/Individuals/Super/). The current contribution is 9% and will increase or decrease according to Australian law. To check your Super you can login online at the [MyGov website](https://my.gov.au/LoginServices/main/login?execution=e1s1)

## Canada

- CXC do not provide private healthcare
- Individuals based in British Columbia will receive 4% vacation pay
- Employer contributions are made to the state pension 
