---
layout: markdown_page
title: "Marketing Programs Management"
---


## On this page
{:.no_toc}

- TOC
{:toc}

## Email Marketing Calendar

The calendar below documents the emails to be sent via Marketo and Mailchimp for:
1. event support (invitations, reminders, and follow ups)
2. ad hoc emails (security, etc.)
3. webcast emails (invitations, reminders, and follow ups)
4. milestones for nurture campaigns (i.e. when started, changed, etc. linking to more details)

*Note: emails in the future may be pushed out if timelines are not met for email testing, receiving lists from event organizers late, etc. The calendar will be updated if the email is pushed out. Please reference the MPM issue boards (described below on this page) to see progress of specific events/webcasts/etc.*

<figure>
  <iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>


## Marketing Programs Managers

Marketing Programs Managers focus on executing, measuring and scaling GitLab's marketing programs such as email campaigns, event promotions, event follow up, drip email nurture series, webinars, and content. Marketing programs also aim to integrate data, personas and content to ensure relevant communications are delivered to the right person at the right time.

## Responsibilities

**Agnes Oetama**
* **Virtual Events**: project management, set up, promotion, and follow up of all virtual events (webcasts, demos, virtual sponsorship)
* **Ad-Hoc Emails**: coordination of copy, review, and set up of one-time emails (i.e. security alert emails, package/pricing changes)
* **Bi-weekly Newsletter**: coordinate with cross-functional teams on topics and set up newsletter in marketo
* **Nurture Campaigns**: strategize and campaigns (email nurturing)
* **International Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Jackie Gragnola**
* **NORAM Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **Gated Content**: implement tracking, review copy, create landing pages, and test flows for gated whitepapers, reports, etc.


**JJ Cordz**
* Cleaning and uploading of lead lists post-event

*Each manager will also own process revamp (including issue template updates and email template design refresh) that falls within their area of resposibility.*

### Order of assignment for execution tasks:
{:.no_toc}
1. Primary responsible MPM  
2. Secondary MPM  (if primary MPM is OOO or not available)
3. Marketing OPS (if both MPMs are OOO or not available)

### Holiday coverage for S1 security vulnerabilities email communication

In the event of an S1 (critical) security vulnerability email communication is needed during the holidays, please create an issue using *[Email-Request-mpm template](https://gitlab.com/gitlab-com/marketing/general/blob/master/.gitlab/issue_templates/Email-Request-mpm.md)* and ping in #marketing-programs tagging @aoetama @jgragnola @jjcordz.

## A visual of what happens when someone fills out a form on a landing page

![](/source/images/marketing-programs/landing-pages-flow-model.png)

## How to read Marketing Programs issue boards

The visual below is a screenshot to demonstrate the information on how to understand the board.

The majority of issues in Jackie's board are related to NORAM conferences, field events, owned events, and gated content. The lists spanning the board move the issues from left to right in relation to their stage of the event prep (tracking, landing pages, invitations, reminder and follow up emails, list upload, and retrospective). You can search the page (ctrl+f) to find an issue and where it is in the process.

The majority of issues in Agnes's board are related to INTERNATIONAL conferences/field events/owned events, virtual events, ad hoc emails, and nurture campaigns. It is also set up proceeding in steps sequentially from left to right.

![](/source/images/marketing-programs/mpm-jg-issue-board.png)

## Marketing Programs labels in GitLab

* **Marketing Programs**: General labels to track all issues related to Marketing Programs. This brings the issue into the board for actioning.
* **MPM - Radar**: Holding place for any issues that will need Marketing Program Manager support, including gated content, events, webcasts, etc.
* **MPM - Supporting Issue Created**: Indicates that the MPM Support issue was created for the MPM - Radar issue. At the time this label is applied, the "MPM - Radar" label will be removed.
* **MPM - Secure presenters and schedule dry runs**: Used when MPM is securing presenters and Q&A support for an upcoming virtual event.
* **MPM - Landing Page & Design**: Used by Marketing Program Manager to indicate that the initiative is in the stage of landing page creation and requesting design assets from the web/design team.
* **MPM - Marketo Flows**: Used by Marketing Program Manager to indicate that the initiative is in the stage of editing/testing of flows in Marketo.
* **MPM - Segmentation & Invitation**: Used by Marketing Program Manager when the initiative is in the stage of identifying segmentation to target and outreach strategy.
* **MPM - Reminders & Follow Up Emails**: Used by Marketing Program Manager when initiative is in the stage of writing and reviewing relevant emails (reminders, follow up, etc.).
* **MPM - List Clean & Upload**: Used by Marketing Program Manager in collaboration with Marketing Ops and Field Marketing manager to receive, clean, and upload event lists for proper tracking and reporting.
* **MPM - Checks & Retrospective**: Used by Marketing Program Manager as final stage to provide information on results and any A/B test findings.
* **MPM - Project**: For non-campaign based optimizations, ideation, and projects of Marketing Program Managers
* **MPM - Blocked/Waiting**: Designates that the MPM is blocked by another team member from moving forward on the issue.
* **MPM - Switch to On-demand**: Used by Marketing Program Manager when switching webcast landing page and subsequent marketo programs to on-demand post event.

## Requesting to "Gate" a Piece of Content

Below is an overview of the process for requesting to put a new piece of content (such as a whitepaper, guide, report, etc.) behind a "gate" aka form on the website.

❌ **A landing page with a form should never be created without the inclusion and testing by Marketing Programs and/or Marketing Ops.**

Please contact Jackie Gragnola @jgragnola if you have any questions.

1. **TEMPLATE:** Create a new issue  using the *[Gated-Content-Request-MPM template](https://gitlab.com/gitlab-com/marketing/general/blob/master/.gitlab/issue_templates/Gated-Content-Request-MPM.md)*
2. **NAME:** The name of the issue should be *Gate Resource: [official name of content]*
3. **WIP:** If the content is in planning state, include *WIP:* until the contents of the piece are determined.
4. **DETAILS:** Fill in relevant details at the top of the issue (requester, type, official content name, and a link to citation policy for analysts)
5. **ASSIGN:** This issue will be automatically assigned to Jackie, who will fill in due dates and alert the proper team members to the next steps needed
6. **WHEN APPROVED:** Jackie will action (i.e. create campaigns, finance tags, set up, test, etc.).

## Requesting Marketing Programs Support for a Field Event

Below is an overview of the process for requesting support for a conference, field event, or owned event. Please contact Jackie Gragnola @jgragnola if you have any questions.

### Overview

The high-level steps are:
1) FMM creates meta issue of event details
2) Budget is approved and WIP is removed from issue
3) Jackie creates MPM Issue with all action items

### Creating the Meta Issue of the Event
* When creating the event meta issue, it will automatically be assigned to Jackie with the "Marketing Programs" tag and "MPM - Radar" tag.
* This will allow the issue to surface in her board, and the MPM issue will be promptly created and next steps defined.
* "WIP" should be at the beginning of the issue until it is approved, at which point the removal of "WIP:" will indicate to Jackie to create the MPM Support Issue.

### Event Channel Types

*[See full campaign progressions here](https://about.gitlab.com/handbook/business-ops/#conference)*

* **Conference:** Any large event that we have paid to sponsor, have a booth/presence at, and are sending representatives from GitLab. 
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of booth visitors post-event. 
  *  Example: DevOps Enterprise Summit, New York City Technology Forum
* **Field Event:** An event that we have paid to participate in but do not own the registration or event hosting duties.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of attendees post-event. 
  *  Hint! If we do not own the registration page for the event, but it is not a conference (i.e. a dinner or breakfast), it is likely a Field Event. Comment in the issue to Jackie if you have need help.
  *  Example: Lighthouse Roadshow (hosted by Rancher), All Day DevOps (virtual event hosted by )
* **Owned Event:** This is an event that we have created, own registration and arrange speaker/venue. 
  *  Note: this is considered an Online Channel for bizible reporting because we manage the registration through our website.
  *  Example: GitLab Day Atlanta, Gary Gruver Roadshow
  *  Example: Lighthouse Roadshow (hosted by Rancher), All Day DevOps (virtual event hosted by )
* **Speaking Session:** This is a talk or speaking session at a conference or field event.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of talk attendees post-event.
  *  Example: Sid & Priyanka's Talk at AWS re:Invent

## Requesting an Email  

Process to request an email can be found in the [Business OPS](https://about.gitlab.com/handbook/business-ops/#requesting-an-email) section of the handbook.   

Primary party responsible for various email types can be determined using the [table above](#responsibilities).   

## Campaign Reporting

MPM uses SFDC reports and dashboards to track program performance real-time. Data from the below SFDC reports/dashboards along with anecdoctal feedback gathered during program retros will be used as guidelines for developing and growing various marketing programs.

The SFDC report/dashboard is currently grouped by program types so MPMs can easily compare and identify top performing and under performing programs within the areas that they are responsible for.

#### [Webcast dashboard](https://na34.salesforce.com/01Z6100000079e6)

The webcast dashboard tracks all webcasts hosted on GitLab's internal webcast plaform. It is organized into 3 columns. The left and middle columns tracks 2 different webcast series (Release Radar vs. CI/CD webcast series). The right column tracks various one-off webcasts since Jan'18.

#### [Live Demo dashboard](https://na34.salesforce.com/01Z6100000079f4)

The LIVE Demo dashboard is organized into 2 columns. The left column tracks the bi-weekly Enterprise Edition product demos (1 hour duration). The bi-weekly Enterprise Edition product demos ran between Q1'18 - Q2'18.
The right column tracks the weekly high level product demo + Q&A session (30 minutes duration). The weekly high level product demo + Q&A session was launched in Q4'18 and currently running through the end of Feb 2019.

#### Virtual Sponsorship dashboard (coming Feb'19)

### Key Metrics tracked in ALL virtual events dashboards

*Note: Virtual Events include Webcast, LIVE demos and Virtual Sponsorship*

**Total Registration :** The number of people that registered for the virtual event regardless whether they attend or not.

**Total Attendance:** The number of people that attended the LIVE virtual event (exclude people who watched the on-demand version).

**Attendance Rate:** % of people that attended the LIVE virtual event out of the total registered (i.e: Total Attendance / Total Registration). 

**Net New Names:** The number of net new names added to our marketing database driven by the virtual event. Because a net new person record may be inserted into our CRM (SFDC) as a lead or a contact object therefore, we need to add `Total net new leads` and `Total net new contacts` to get the overall total net new names.

**Influenced Pipe:** Total New and Add-on business pipeline IACV$ influenced by people who attended the LIVE virtual event. The webcast and live demo dashboards currently use SFDC out of the box `Campaigns with Influenced opportunities` report type because Bizible was implemented in June'18 and therefore the attribution report did not capture data prior to this. We plan to migrate webcast and live demo influenced pipe reports to Bizible attribution report in the next dashboard iteration so they align with overall marketing reporting.

## Program logistical set up

### Webcast

Webcast program set up can be found in the [Business OPS](https://about.gitlab.com/handbook/business-ops/#logistical-setup) section of the handbook.

### Newsletter

#### Creating the newsletter in Marketo

A day or two before the issue due date, create the newsletter draft. It's easiest to clone the last newsletter in Marketo:

1. Go to Marketing Activities > Master Setup > Outreach > Newsletter & Security Release
1. Select the newsletter program template `YYYYMMDD_Newsletter Template`, right click and select `Clone`.
1. Clone to `A Campaign Folder`.
1. In the `Name` field enter the name following the newsletter template naming format `YYYYMMDD_Newsletter Name`.
1. In the `Folder` field select `Newsletter & Security Release`. You do not need to enter a description.
1. When it is finished cloning, you will need to drag and drop the new newsletter item into the appropriate subfolder (`Bi-weekly Newsletters`, `Monthly Newsletters` or `Quarterly Newsletters`).
1. Click the + symbol to the left of your new newsletter item and select `Newsletter`.
1. In the menu bar that appears along the top of your screen, select `Edit draft`.

#### Editing the newsletter in Marketo

1. Make sure you update the subject line.
1. Add your newsletter items by editing the existing boxes (double click to go into them). It's best to select the `HTML` button on the menu bar and edit the HTML so you don't inadvertently lose formatting.
1. Don't forget to update the dates in the UTM parameters of your links (including the banner at the top and all default items such as the "We're hiring" button).

#### Sending newsletter test/samples from Marketo

1. When you're ready, select `Email actions` from the menu at the top, then `Send sample` to preview.
1. Enter your email in the `Person` field, then in `Send to` you can add any other emails you'd like to send a preview too. We recommend sending a sample to the newsletter requestor (or rebecca@ from the content team for marketing newsletters) for final approval.
1. When you are satisfied with the newsletter, select `Approve and close` from the `Email actions` menu.

#### Sending the newsletter

1. When the edit view has closed, click on the main newsletter item in the left-hand column.
1. In the `Schedule` box, enter the send date and select `Recipient time zone` if the option is available.
1. Make sure `Head start` is checked too.
1. In the `Approval` box, click on `Approve program`.
1. Return to the newsletter issue and leave a comment telling requestor (@rebecca from the content team for marketing newsletters)  to double check all has been set up correctly. Close the issue when this is confirmed.
